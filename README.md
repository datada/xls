Nitro (python)
======================

## 1 From REPL, read and write to Excel Spreadsheet

```py
>>> Cell("A1").value
42
>>> Cell("A1").value = "Hello"
>>> CellRange("A1:F13").color = range(0,234,3)
```

## 2 Run stock_chart.py script behind Excel Spreadsheet

When the user enters "APPL" in the celll "B1", the script updates parts of the spreadsheet.

```py
def price_data(arg):
	return "..."

while 1:
	if Cell("chart", "B1").value != ticker:
		sleep(0,1)
		ticker = Cell("chart", "B1").value
		price_data(ticker)
```

## 3 Provide user defined functions.

fuctions.py

```py
def f():
	return "data"
```

When the definition changes, the cell is updated.


xlwt (python)
=============

```py
from tempfile import TemporayFile
from xlwt import Workbook

book = Workbook()
sheet1 = book.add_sheet("Sheet 1")
book.add_sheet("Sheet 2")

sheet1.write(0,0,"A1")
sheet1.write(0,1,"B1")
row1 = sheet1.row(1)
row1.write(0, "A2")
row1.write(1, "B2")
sheet1.col(0).width = 10000

sheet2 = book.get_sheet(1)
sheet2.row(0).write(0, "Sheet 2 A1")
sheet2.row(0).write(1, "Sheet 2 B1")
sheet2.flush_row_data()
sheet2.write(1, 0, "Sheet 2 A3")
sheet2.col(0).width = 5000
sheet2.col(0).hidden = True

book.save("simple.xls")
book.save(TemporaryFile())
```


F#
==

## e.g. from somewhere over the net

```ml
open Microsoft.Office.Interop.Excel
open System
open System.Runtime.InteropServices //for COMException

let app = new ApplicationClass(Visible = true)
let workbooks = app.Workbooks
let workbook = workbooks.Add(XlWBATemplate.xlWBATWorksheet)
let sheets = workbook.Worksheets
let worksheet = (sheets.[box 1] :?> _Worksheet)

Console.WriteLine ("Setting the value for cell")

worksheet.Range("G1").Value2 <- 5

// single dim array to Excel
worksheet.Range("A1", "E1").Value2 <- [| for i in 0 .. 4 -> i * i |]
worksheet.Range("A2", "E2").Value2 <- [| for i in 0 .. 4 -> sin (float i) |]

// 2 dim array to Excel
let array3 = Array2D.init 4 5 (fun i j -> i*10 + j)
worksheet.Range("A3", "E6").Value2 <- array3

// reads 2 dim array from Excel
let array4 = (worksheet.Range("A3", "E6").Value2 :?> obj[,])

printf "Low: %d\n" (array4.GetLowerBound(0))

for i=array4.GetLowerBound(0) to array4.GetUpperBound(0) do
	for j=array4.GetLowerBound(1) to array4.GetUpperBound(1) do
		printf "Compare %d, %d " i j;
		if int (Array2D.get array4 i j :?> float) <> Array2D.get array3
		(i-array4.GetLowerBound(0)) (j-array4.GetLowerBound(1)) then
			Console.WriteLine ("ERROR: Comparison FAILED!")

// fill 2 dim array with points for 2 curves and send to Excel
let range5 = worksheet.Range("A8", "J9")
let array5 =
	Array2D.init 2 10
		(fun i j ->
			let arg = (Math.PI / 10.0) * float j
			if i = 0 then Math.Sin(arg) else Math.Cos(arg))
range5.Value2 <- array5

//draw chart fails due to Excel/CLR bug
range5.Select() |> ignore

let chartobjects = (worksheet.ChartObjects() :?> ChartObjects)
let chartobject = chartobjects.Add(10.0, 100.0, 450.0, 250.0)

// chart
chartobject.Chart.ChartWizard(Source = range5,
							  Gallery = XlChartType.xl3DColumn,
							  PlotBy = XlRowCol.xlRows,
							  HasLegend = true,
							  Title = "Sample Chart",
							  CategoryTitle = "Sample Category Type",
							  ValueTitle = "Sample Value Type")
chartobject.Visible <- false
chartobject.Visible <- true
chartobject.Parent |> ignore
chartobject.Chart.GetType() |> ignore

#if COMPILED
System.Threading.Thread.Sleep(1000)

// explicit close
let _ =
	try 
		workbook.Saved <- true
		app.UserControl <- false
		app.Quit()
	with e -> Console.WriteLine ("User closed Excel manually, so we don't have to")

let _ = Console.WriteLine ("Sample finished!")
#endif
```

## F# Succintly Chart e.g.

```ml
#load "FSharpChart.fsx"
open System
open MSDN.FSharp.Charting

let data = [1;2;3;4]
FSharpChart.Line data
```

## Visicalc in Scheme

ss.scm



