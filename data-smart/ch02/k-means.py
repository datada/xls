import random
import numpy as np

# based on Data Smart Ch 2 and http://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/


# [Customer Offer] -> {Customer:[Offer]} -> [ Customer numpy.array([]) ] where Customer is String, Offer is Int
# group the offers for customer
def group_offers_for_each_customer(rows, size):
    customer_offers = {}
    for row in rows:
        customer = row[0]
        offer = int(row[1])
        if not customer_offers.get(customer, None):
            customer_offers[customer] = [0 for i in range(size)]
        customer_offers[customer][offer - 1] = 1 #offer ranges from 1
    return [(k, np.array(v)) for k, v in customer_offers.items()]


# [Customer [Offer]] -> [[Offer]]
def offers_only(xs):
    return [x[1] for x in xs]


# coulda used from scipy.spatial.distance import euclidean
def euclid_distance(xs, ys):
    return np.linalg.norm(xs-ys)


# [Num] [(Customer [Offer])] -> {Int: [Customer:[Offer]]}
def recluster(centers, data):
    clusters = {}
    for customer, offers in data:
        # print "customer", customer
        # print "offers", offers
        best_m_key = min([(i, euclid_distance(offers, center)) for i, center in enumerate(centers)], key=lambda t: t[1])[0]
        # print "best culster", best_m_key
        if not clusters.get(best_m_key, None):
            clusters[best_m_key] = []
        clusters[best_m_key] = clusters[best_m_key] + [(customer, offers)]
    # print "reclustered", clusters
    return clusters


# [Num] {Int: [(Customer, [Offer])]} -> [Num]
def recenter(centers, clusters):
    for k in sorted(clusters.keys()):
        cluster = clusters[k]
        answer = np.mean([offers for customer, offers in cluster], axis = 0)
        # print "min distance", answer
        yield answer


# [Num] [Num] -> Bool
def has_converged(xs, ys):
    return set([tuple(x) for x in xs]) == set([tuple(y) for y in ys])


# [(Label [Int])] Int -> [[Num] {Int: [(Label [Int])]}
def find_k_means(data, k):
    xs = [offers for (label, offers) in data]
    old_mus = random.sample(xs, k)
    mus = random.sample(xs, k)
    cnt = 0
    while not has_converged(mus, old_mus):
        cnt += 1
        print "iteration", cnt
        old_mus = mus 
        clusters = recluster(mus, data)
        # print "clusters", clusters
        mus = [mu for mu in recenter(mus, clusters)]
        # print "mus", mus
    return (mus, clusters)


def main():
    # load data
    from shared import load_offers, load_purchases
    cols_of_offers, rows_of_offers = load_offers()
    purchases = load_purchases(size_of_offers=len(rows_of_offers))

    # calculate
    (centers, clusters) = find_k_means(purchases, 4)

    # report
    template = "{0:8}|{1:10}|{2:15}|{3:20}|{4:15}|{5:10}|{6:10}"
    for k, center in enumerate(centers):
        print "Cluster", k
        print template.format(*cols_of_offers)
        for top in sorted(zip(center, rows_of_offers), key=lambda pair: pair[0])[-4:]:
            print template.format(*top[1])
        print ""


if __name__ == '__main__':
    main()