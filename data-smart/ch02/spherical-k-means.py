from scipy.spatial.distance import cosine
import itertools
import numpy as np
import random


# [Num] [Num] -> Num
def cosine_distance(xs, ys):
    if 0 == np.linalg.norm(xs) or 0 == np.linalg.norm(ys):
        return 1
    return cosine(xs, ys)


# [Num] [Num] -> Bool
def has_converged(xs, ys):
    return set([tuple(x) for x in xs]) == set([tuple(y) for y in ys])


# [Vec] [Vec] (Vec Vec -> Num) -> Vec
def argmin_of_distance(xs, ys, distance):
    distances = [(x, sum([distance(x, y) for y in ys])) for x in xs]
    # print "distnaces", distances
    return min(distances, key=lambda t: t[1])[0]


# [Num] {Int: [(Customer, [Offer])]} -> [Num]
def recenter(centers, clusters):
    for k in sorted(clusters.keys()):
        # find the [0 1 ...] that minimizes the sum of cosine distances
        # print "recenter", k
        cluster = clusters[k]
        ys = [offers for customer, offers in cluster]
        # print "find argmin of sum of distance for", ys
        # cutting corners here, instead of searcing all possible combination of offers,
        # one of the real data points
        answer = np.array(argmin_of_distance(ys, ys, cosine_distance))
        # print "min distance", answer, "for cluster", k
        yield answer


# [Num] [(Customer [Offer])] -> {Int: [Customer:[Offer]]}
def recluster(centers, data):
    clusters = {}
    for customer, offers in data:
        # print "customer", customer
        # print "offers", offers
        best_m_key = min([(i, cosine_distance(offers, center)) for i, center in enumerate(centers)], key=lambda t: t[1])[0]
        # print "best culster", best_m_key
        if not clusters.get(best_m_key, None):
            clusters[best_m_key] = []
        clusters[best_m_key] = clusters[best_m_key] + [(customer, offers)]
    # print "reclustered", clusters
    return clusters


# [(Label [Int])] Int -> [[Int] {Int: [(Label [Int])]}
def find_spherical_k_means(data, k):
    xs = [offers for (label, offers) in data]
    old_centers = random.sample(xs, k)
    centers = random.sample(xs, k)
    cnt = 0
    while not has_converged(centers, old_centers):
        cnt += 1
        print "iteration", cnt
        old_centers = centers
        clusters = recluster(centers, data)
        centers = [center for center in recenter(centers, clusters)]
    return centers, clusters


def test():
    assert has_converged([[0, 0, 1], [0, 1, 0]], [[0, 0, 1], [0, 1, 0]])
    assert cosine_distance([1, 1], [0, 0]) == 1
    v = [0, 1, 1]
    assert argmin_of_distance(itertools.product([0, 1], repeat=len(v)), [v], cosine_distance) == tuple(v)


def main():
    from shared import load_offers, load_purchases
    cols_of_offers, rows_of_offers = load_offers()
    # print cols_of_offers, rows_of_offers[:5]
    purchases = load_purchases(size_of_offers=len(rows_of_offers))
    # print purchases[:5]
    (centers, clusters) = find_spherical_k_means(purchases, k=5)

    print "Summary"
    template = "{0:8}|{1:10}|{2:15}|{3:20}|{4:15}|{5:10}|{6:10}"
    for k, center in enumerate(centers):
        print "Cluster", k
        print template.format(*cols_of_offers)
        offers_of_interest = [rows_of_offers[k] for k,v in enumerate(center) if v == 1]
        for xs in offers_of_interest:
            print template.format(*xs)


if __name__ == '__main__':
    test()
    main()
