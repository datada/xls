import nltk
import csv


# to be reset with the content of *.csv file
docs = [
    (["Chinese", "Beijing", "Chinese"], "c"),
    (["Chinese", "Chinese", "Shanghai"], "c"),
    (["Chinese", "Macao"], "c"),
    (["Tokyo", "Japan", "Chinese"], "j"),
]
with open("Tweets.csv", 'rb') as f:
    gen = csv.reader(f)
    head = gen.next()
    docs = [(row[1].split(" "), row[1]) for row in gen]


all_words = []
for (ws, c) in docs:
    all_words += ws


def doc_features(doc):
    doc_words = set(doc)
    features = {}
    for word in all_words:
        features["contains({0})".format(word)] = (word in doc_words)
    return features

train_set = nltk.classify.apply_features(doc_features, docs)
classifier = nltk.NaiveBayesClassifier.train(train_set)

for doc in docs:
    print "actual=", doc[1], "predicted=", classifier.classify(doc_features(doc[0]))

