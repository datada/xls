from scipy.spatial.distance import cosine
import numpy as np
from shared import close_enough


# [Num] [Num] -> Num
def cosine_similarity(xs, ys):
    if 0 == np.linalg.norm(xs) or 0 == np.linalg.norm(ys):
        return 0
    cs = -1 * (cosine(xs, ys) - 1.0)
    if cs <= 0:
        # prevent -0.0
        return
    return cs


# [(String, [Binary])] -> {(String, String): Num}
def cosine_similarity_btw(data):
    return {(name, another_name): cosine_similarity(xs, ys)
            for (name, xs) in data
                for (another_name, ys) in data}


# [[]] String -> IO
def write_to_csv(data, fname, delimiter=","):
    import csv
    with open(fname, "wb") as fp:
        a = csv.writer(fp, delimiter=delimiter)
        return a.writerows(data)


# {(Name, Name): Num} [Names] -> [[Val]]
def build_rows(data, names):
    yield [''] + names
    for name in names:
        yield [name] + [data[(name, other)] for other in names]


def main():
    from shared import load_offers, load_purchases
    import math
    cols_of_offers, rows_of_offers = load_offers()
    # print cols_of_offers, rows_of_offers[:5]
    purchases = load_purchases(size_of_offers=len(rows_of_offers))
    # [(String, [Binary])]
    # print purchases[:5]
    names = sorted([name for (name, _) in purchases])

    csm = cosine_similarity_btw(purchases)

    # self similarity s/b 1 but we change to 0 because we already know this and not useful
    for name in names:
        assert "1.0" == str(csm[(name, name)])
        csm[(name, name)] = 0

    # necessary in Excel work flow but not that useful in python
    rows = build_rows(csm, names)
    write_to_csv(rows, "cosine-similarity-matrix.csv")
    assert math.sqrt((0.408248 - csm[("Adams", "Bailey")])**2) < 0.0001
    assert math.sqrt((0.408248 - csm[("Adams", "Bennett")])**2) < 0.0001
    assert math.sqrt((0.666667 - csm[("Adams", "Brown")])**2) < 0.0001
    assert math.sqrt((0.258199 - csm[("Adams", "Butler")])**2) < 0.0001
    assert math.sqrt((0.408248 - csm[("Adams", "Collins")])**2) < 0.0001
    assert math.sqrt((0.816497 - csm[("Adams", "Cruz")])**2) < 0.0001
    assert math.sqrt((0.5 - csm[("Barnes", "Baker")])**2) < 0.0001

    # 0.5 neighborhood graph from cosine-similarity
    adj = {(name, other): (1 if 0.4999 <= csm[(name, other)] else 0)
            for name in names
                for other in names}

    rows = list(build_rows(adj, names))
    write_to_csv(rows, "adjacency-matrix.csv")
    assert 1 == adj[("Torres", "Wilson")]
    assert 0 == adj[("Torres", "Young")]
    assert 1 == adj[("Ward", "Wright")]

    stubs_of_customer = {}
    total_stub = 0
    for row in rows[1:]:
        name = row[0]
        subtotal = sum(row[1:])
        stubs_of_customer[name] = subtotal
        total_stub += subtotal

    for name in names:
        assert stubs_of_customer[name] is not None

    # Parker is a known loner
    assert 0 == stubs_of_customer["Parker"]
    assert 15 == stubs_of_customer["Torres"]
    assert total_stub == sum(v for k, v in stubs_of_customer.items())
    assert total_stub == 858

    scorer = lambda name, other: adj[(name, other)] - 1.0 * stubs_of_customer[name] * stubs_of_customer[other] / total_stub
    scores = {(name, other): scorer(name, other)
            for name in names
                for other in names}
    print scores

    rows = build_rows(scores, names)
    assert close_enough(-0.228, scores[("Adams", "Adams")])
    assert close_enough(0.755, scores[("Adams", "Brown")])
    write_to_csv(rows, "score-matrix.csv")


if __name__ == '__main__':
    main()
