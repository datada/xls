import numpy as np
import csv
from scipy import optimize # does not do integer programming


names = []
adj = {}
with open("adjacency-matrix.csv", 'rb') as f:
    gen = csv.reader(f)
    head = gen.next()
    names = head[1:]
    for row in gen:
        adj[row[0]] = [int(v) for v in row[1:]]


scores = {}
with open("score-matrix.csv", 'rb') as f:
    gen = csv.reader(f)
    head = gen.next()
    for row in gen:
        scores[row[0]] = [float(v) for v in row[1:]]


# [Binary] -> [Num]
def modularity_score(community_assignments):
    total = 0
    for k, v in enumerate(community_assignments):
        name = names[k]
        # if name is 1, then we want the scores of all 1's, else 0's
        neighbors = [1 if v == assignment else 0 for assignment in community_assignments]
        total += np.dot(neighbors, scores[name])
    return total


def objective(x):
    return -1 * modularity_score(x)


def minimize(fn, init, constraint):
    # ToDo
    return None


if __name__ == '__main__':
    print names
    for k, v in adj.items():
        print k, v

    for k, v in scores.items():
        print k, v

    x0 = [0 for name in names]
    print x0

    result = minimize(objective, x0, [])
