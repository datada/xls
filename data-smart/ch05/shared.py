import csv
import numpy as np


# Number Number -> Bool
def close_enough(a, b, eps=0.001):
    return abs(a - b) < eps


# String [String]* -> [[Any]]
# e.g. [["cName","state","enrollment"],["Stanford","CA","15000"] ...]
def rows_of_csv(fpath, names=None):
    with open(fpath, 'rb') as f:
        if names:
            yield [names]
        for row in csv.reader(f):
            yield row


# -> (Head, [Row])
def load_offers():
    gen = rows_of_csv("OfferInformation.csv")
    head = gen.next()
    return head, [[int(row[0])] + row[1:] for row in gen]


# Int -> [(String, [Int])]
# e.g. ('Smith', array([0, 1, ... means that Smith purchased offer 2
def load_purchases(size_of_offers):
    gen = rows_of_csv("Transactions.csv")
    head = gen.next()
    rows = [(row[0], int(row[1])) for row in gen]
    d = {}
    for row in rows:
        customer, offer = row
        if not d.get(customer, None):
            d[customer] = [0] * size_of_offers
        # caution: offer id ranges from 1
        d[customer][offer-1] = 1
    return [(customer, np.array(offers)) for customer, offers in d.items()]


# show the data
def main():
    cols_of_offers, rows_of_offers = load_offers()
    print cols_of_offers, rows_of_offers[:5]
    purchases = load_purchases(size_of_offers=len(rows_of_offers))
    print purchases[:5]


if __name__ == '__main__':
    #main()
    load_scores()