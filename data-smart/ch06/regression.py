from scipy.optimize import leastsq
import numpy as np
import csv


head = []
xs = []
ys = []
with open("RetailMart.csv", 'rb') as f:
    gen = csv.reader(f)
    head = gen.next()[:-1]
    for row in gen:
        xs.append([int(v) for v in row[:-1]])
        ys.append(int(row[-1]))

print xs
print ys
cs = [0 for x in head]


# [Number] -> [Number]
# vector of size N -> M floating numbers
def residual(coefs):
    return [y - np.dot(coefs, x) for x, y in zip(xs, ys)]


rslt = leastsq(residual, x0=cs)

for p, v in zip(head, rslt[0]):
    print p, v

