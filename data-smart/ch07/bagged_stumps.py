import csv
import operator
import random


head = []
xs = []
ys = []
with open("RetailMart.csv", 'rb') as fin:
    gen = csv.reader(fin)
    head = gen.next()[:-1]
    for row in gen:
        xs.append([int(v) for v in row[:-1]])
        ys.append(int(row[-1]))

# print xs
# print ys


# [[Number]] [Number] Int -> [[Number]] [Number] [Int]
# random subset for picking stump
def randomly(xs, ys, size):
    cs = random.sample([k for k, v in enumerate(xs[0])], size)

    new_xs = []
    new_ys = []
    for x, y in random.sample(zip(xs, ys), int(len(ys) * 2 / 3)):
        new_xs.append(x)
        new_ys.append(y)
    return new_xs, new_ys, cs


# Int Int Int Int -> Number
def impurity(a, b, c, d):
    e = 1.0 - (a/(a + b)) ** 2 - (b/(a + b)) ** 2 if 0 < (a + b) else 1
    f = 1.0 - (c/(c + d)) ** 2 - (d/(c + d)) ** 2 if 0 < (c + d) else 1
    return (e * (a + b) + f * (c + d))/sum([a, b, c, d])


def pregnancy_indicator(a, b, c, d):
    indicator = 0
    p_per_zero = 1.0 * a/(a + b) if 0 < (a + b) else 0
    p_per_one = 1.0 * c/(c + d) if 0 < (c + d) else 0
    if p_per_zero < p_per_one:
        # 1 results in higher pregnancy
        indicator = 1
    return indicator


def test_pregnancy_indicator():
    assert 1 == pregnancy_indicator(299, 330, 34, 3)
    assert 0 == pregnancy_indicator(315, 254, 18, 79)
    assert 1 == pregnancy_indicator(252, 324, 71, 9)
    assert 1 == pregnancy_indicator(293, 325, 40, 8)


# Int -> (Probability, Binary)
def run_stump(xs, ys, c):
    # (predictor pregnancy)
    bucket = {
        (0, 1): 0, #a
        (0, 0): 0, #b
        (1, 1): 0, #c
        (1, 0): 0  #d
    }
    for x, y in zip(xs, ys):
        bucket[(x[c], y)] += 1

    a = bucket[(0, 1)]
    b = bucket[(0, 0)]
    c = bucket[(1, 1)]
    d = bucket[(1, 0)]

    return impurity(a, b, c, d), pregnancy_indicator(a, b, c, d)


def test_run_stump():
    # 8 is Folic Acid
    impurity, preg_ind = run_stump(xs, ys, 8)
    assert 1 == preg_ind


# [Int] -> Int
def winner(xs, ys, cs):
    # where r = (Probability, Binary)
    rs = [run_stump(xs, ys, c) for c in cs]
    impurities = [r[0] for r in rs]
    index, value = min(zip(cs, impurities), key=operator.itemgetter(1))
    return index, rs[impurities.index(value)][1]


def tests():
    test_pregnancy_indicator()
    test_run_stump()


def vote(winners, x):
    return [1 if pi == x[index] else 0 for index, pi in winners]


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step


def main():
    winners = []
    for i in range(200):
        _xs, _ys, _cs = randomly(xs, ys, 4)
        print "candidates", _cs
        win = winner(_xs, _ys, _cs)
        print "winner", win
        winners.append(win)

    a_p = []
    for x, y in zip(xs, ys):
        votes = vote(winners, x)
        prediction = sum(votes) / float(len(votes))
        print "actual=", y, "prediction=", prediction
        a_p.append((y, prediction))

    for v in drange(0.02, 0.75, 0.02):
        true_positive = 0
        false_positive = 0
        false_negative = 0
        true_negative = 0
        for a, p in a_p:
            if a == 1 and v <= p:
                true_positive += 1
            if a == 1 and p < v:
                false_negative += 1
            if a == 0 and v <= p:
                false_positive += 1
            if a == 0 and p < v:
                true_negative += 1
        print "for cutoff=", v
        print "true_positive", true_positive
        print "false_positive", false_positive
        print "true_negative", true_negative
        print "false_negative", false_negative


if __name__ == '__main__':
    tests()
    main()
