from shared import load_support_personnel, close_enough
from scipy.spatial.distance import euclidean


# [(val, mean, std)] -> [val]
def standardize(xs):
    return [(val - mean) / std for val, mean, std in xs]


# [Row] String:[Num] String:[Num] -> [Row]
def normalize(rows, means, stds):
    # first element is label
    return [[row[0]] + standardize(zip(row[1:], means[1:], stds[1:])) for row in rows]


# -> {(ID ID):Number}
def distance_btw_personnel():
    cols, rows, means, stds = load_support_personnel()
    normalized_rows = normalize(rows, means, stds)
    distances = {}
    for row in normalized_rows:
        eid = row[0]
        vals = row[1:]
        for other_row in normalized_rows:
            other_eid = other_row[0]
            other_vals = other_row[1:]
            distances[(eid, other_eid)] = euclidean(vals, other_vals)

    return distances


# {(ID ID):Number} -> {ID:[Number]}
def neighbors(d):
    # {ID:[Number]} all distances for a given ID
    bucket = {}
    for k, v in d.items():
        eid, other = k
        if eid in bucket:
            bucket[eid].append((v, other))
        else:
            bucket[eid] = [(v, other)]
    # distances are sorted from low to high
    return {k: sorted(v) for k, v in bucket.items()}


def main():
    dist_btw = distance_btw_personnel()
    for k, v in dist_btw.items():
        print k, v
        if k[0] == k[1]:
            assert v < 0.01
    ns = neighbors(dist_btw)
    for k, v in ns.items():
        print k, v
        assert 0.0 == v[0][0]
        assert k == v[0][1]



if __name__ == '__main__':
    main()
