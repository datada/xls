from calc_distance import distance_btw_personnel, neighbors


# {(ID ID):NUMBER} {ID:[(Number ID)]} -> {(ID ID):Int}
def rank_btw_personnel(d, ns):
    return {k: ns[k[0]].index((v, k[1])) for k, v in d.items()}


# {(ID ID):Int} -> {ID:Int}
def knn(ranks, k):
    bucket = {}
    for key, v in ranks.items():
        eid = key[1]
        if eid not in bucket:
            bucket[eid] = 0
        if 0 < v < (k + 1):
            bucket[eid] += 1
    return bucket


# {ID:[Number]} Int Int -> Number
def knn_dist(neigh_dist, k, eid):
    return neigh_dist[eid][k][0]


def main():
    dist_btw = distance_btw_personnel()
    ns = neighbors(dist_btw)

    rank_btw = rank_btw_personnel(dist_btw, ns)
    for k, v in rank_btw.items():
        eid, other = k
        if eid == other:
            assert v == 0

    knn_20 = knn(rank_btw, 20)
    print "outlier (k=20)", "k-distance (k=5)"
    for eid, v in knn_20.items():
        if v == 0:
            # 143406 and 137155 are outliers and do not appear in knn of others
            print eid, knn_dist(ns, 20, eid)


if __name__ == '__main__':
    main()
