from calc_distance import distance_btw_personnel, neighbors


# Int Int {Int:[(Number, Int)} -> Number
def k_distance(eid, k, ns):
    return ns[eid][k][0]


# {(ID ID):Number} {Int:[(Number Int)]} -> {(Int Int):Number}
def reach_distances(dist, ns):
    bucket = {}
    for k, v in dist.items():
        eid, other = k
        bucket[k] = max(v, k_distance(other, 5, ns))
    return bucket


# {(ID ID):Number} {Int:[(Number Int)]} Int -> {Int:Number}
def avg_reach_distances(rd, ns, k):
    bucket = {}
    for eid, v in ns.items():
        bucket[eid] = 0
        # top k-th neighbors
        for dist, other in v[1:k+1]:
            bucket[eid] += rd[(eid, other)]
        bucket[eid] /= k
    return bucket


# {ID:[(Number ID)] {ID:Number} Int -> {Int:Number}
def local_outlier_factors(ns, ard, k):
    bucket = {}
    for eid, v in ns.items():
        bucket[eid] = 0
        # top k-th neighbors
        for dist, other in v[1:k+1]:
            bucket[eid] += ard[eid] / ard[other]
        bucket[eid] /= k
    return bucket


def main():
    dist_btw = distance_btw_personnel()
    ns = neighbors(dist_btw)

    print k_distance(143406, 5, ns), "k-distance (k=5) of", 143406

    reach_ds = reach_distances(dist_btw, ns)
    for k, v in reach_ds.items():
        print k, v

    avg_rd = avg_reach_distances(reach_ds, ns, 5)
    for k, v in avg_rd.items():
        print k, v

    print "========= Local Outlier Factors =========="
    lof = local_outlier_factors(ns, avg_rd, 5)
    for k, v in lof.items():
        # 143406 1.97
        # 137155 1.74
        if 1.5 < v:
            print k, v


if __name__ == '__main__':
    main()
