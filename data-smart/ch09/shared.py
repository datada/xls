import csv


# Number Number -> Bool
def close_enough(a, b, eps=0.001):
    return abs(a - b) < eps


# String [String]* -> [[Any]]
# e.g. [["cName","state","enrollment"],["Stanford","CA","15000"] ...]
def rows_of_csv(fpath, names=None):
    with open(fpath, 'rb') as f:
        if names:
            yield [names]
        for row in csv.reader(f):
            yield row


# "90.2%" -> .902
def p2f(x):
    return float(x.strip('%'))/100


# Any -> Any
def convert(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            if "%" in s:
                return p2f(s)
    return s


# [Any] -> [Any]
def convert_row(xs):
    return [convert(x) for x in xs]


# -> (Head, [Row])
def load_support_personnel():
    #last two are Mean and STD
    gen = rows_of_csv("SupportPersonnel.csv")
    head = gen.next()
    body = [convert_row(row) for row in gen]
    means = body[-2]
    stds = body[-1]
    rows = body[:-2]

    return head, rows, means, stds


# show the data
def main():
    assert close_enough(4.84207, 4.845, eps=0.01)
    cols, rows, means, stds = load_support_personnel()
    print cols
    print rows[:5]
    print means
    print stds


if __name__ == '__main__':
    main()
