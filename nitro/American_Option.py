from math import exp, sqrt, log
import datetime

def up_step(vol, t, steps): return exp(vol * sqrt(float(t)/steps))
#Amount by which the stock goes up in one time step

def down_step(vol, t, steps): return exp(-vol * sqrt(float(t)/steps))
#Amount by which the stock goes down in one time step
    
def prob(r, q, t, steps, up, down): return (exp((r-q)*float(t)/steps) - down)/(up - down)
#Probability of a move up, per time step. Based on a risk-neutral valuation.

def price_tree(S, up, down, steps, t):
    clear_sheet("Stock Price")
    active_sheet("Stock Price")
    dividends(steps, t)
    Cell(2 + steps, 1).value = S
    for col in xrange(2, steps + 2):
        first_row = steps + 3 - col
        div = Cell(1, col).value
        x = Cell(first_row, col)
        x.value = max(up * x.offset(1,-1).value - div, 0)
        for row in xrange(first_row + 2, first_row + 2*col, 2):
            x = Cell(row, col)
            x.value = max(down * x.offset(-1, -1).value - div, 0)

def opt_tree(k, steps, p, is_call):
    clear_sheet("Tree")
    active_sheet("Tree")
    tree_init(k, steps, is_call)
    for col in xrange(steps, 0, -1):
        for row in xrange(steps + 3 - col, steps + 3 + col, 2):
            early_exercise = Cell("Stock Price", row, col).value - k
            if not is_call: early_exercise *= -1 # negate for put
            x = Cell(row, col)
            x.value = max(p*x.offset(-1, 1).value + (1-p)*x.offset(1,1).value, early_exercise)

def dividends(steps, t):
    Cell("A1").value = "Dividends:"
    time_step = float(t)/steps

    next_div = Cell("Call_or_put","first_div").value / 365.0 #time from now
    freq = float(t)/Cell("Main", "div_per_yr").value

    for i in xrange(1, steps+1):
        this_div = 0
        div = Cell("Main", "div").value
        while i*time_step > next_div:
            this_div += div
            next_div += freq
        Cell(1, i+1).value = this_div

    CellRange((1,1),(1,steps+1)).font.color = 'gray'
    CellRange((1,1),(1,steps+1)).font.italic = True

def tree_init(k, steps, is_call):
    maxcol = steps + 1
    x = CellRange((1,1),(1,maxcol))
    x.value = CellRange("Stock Price", (1,1),(1,maxcol)).value
    x.font.color, x.font.italic = 'gray', True
    for row in xrange(2, 2*maxcol + 1, 2):
        if is_call: # Call Price = Max(0, S-K)
            Cell(row, maxcol).value = max(Cell("Stock Price", row, maxcol).value - k, 0)
        else: #Put Price = Max(S-K, 0)
            Cell(row, maxcol).value = max(k - Cell("Stock Price", row, maxcol).value, 0)

### Main Script
active_sheet("Main")
S, div_per_yr, K, r, t, steps = CellRange('init').value 
up = up_step(float(Cell('vol').value), t, steps)
down = down_step(float(Cell('vol').value), t, steps)
q = div_per_yr * log(1.0 + Cell('div').value / (float(S) * div_per_yr))
p = prob(r, q, t, steps, up, down)
is_call = (Cell("call_or_put").value == 'Call')

price_tree(S, up, down, steps, t)
opt_tree(K, steps, p, is_call)
Cell("Main","american_price").value = round(Cell("Tree", steps + 2, 1).value, 2)
