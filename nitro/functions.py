# -*- coding: utf-8 -*-
import scipy, scipy.linalg

def matrix_exponent(matrix, row, col):
        x = scipy.array(matrix)
	ans = scipy.linalg.expm(x)
	return ans[int(row)][int(col)]

def foo():
    return "foo"

    
if __name__ == '__main__':
	print foo()
