# Generates approximations of pi.  Put the number of trials you want in A1. More
# trials will result in higher accuracy.

from random import random
from time import sleep

def pi_calc(n):
    while Cell("B2").value < n:
        x, y = Cell("D2"), Cell("E2")
        r = Cell("F2")
        x.value, y.value = random(), random()
        r.value = x.value**2 + y.value**2
        Cell("B2").value += 1
        if r.value <= 1:
            Cell("B3").value += 1
        Cell("B4").value = 4*float(Cell("B3").value)/Cell("B2").value
        sleep(0.01)

trials = Cell("A1").value
if (type(trials)!= int) or (trials < 1):
    trials = 100

# sheet setup
Cell("A2").value, Cell("A3").value, Cell("A4").value = "Trial:","Hits:","pi is about:"
Cell("D1").value, Cell("E1").value, Cell("F1").value = "x","y","x^2 + y^2"
Cell("B2").value, Cell("B3").value = 0,0

pi_calc(trials)
