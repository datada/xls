import numpy as np 
import pandas as pd 
from pandas import DataFrame, Series

df = pd.read_csv('00-data.csv')

# grouped = df.groupby("Region")

# for name, group in grouped:
# 	print name
# 	print group

# similar to what Excel does except the margin sums
def one():
	return df.groupby(["Region", "Product"])["Revenue"].sum().unstack()

# add a column that sums across/horizontal
def two():
	mydf = one()
	mydf["Grand Total"] = mydf.apply(lambda x: x.sum(), axis=1)
	return mydf

# creates the "bottom row" that sums vertical
def three():
	mydf = two()
	bottom_row = mydf.apply(lambda x: x.sum())
	return DataFrame({key:bottom_row[key] for key in bottom_row.index}, index=["Grand Total"], columns=mydf.columns)

# put two on top of thee gets you what Excel produces
def four():
	return pd.concat([two(), three()])