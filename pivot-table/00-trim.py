import csv

# Excel spat out empty columns and rows
# data-org.csv -> data.csv

def is_all_blank_line(xs):
	for x in xs:
		if x == '':
			pass
		else:
			return False
	return True

def non_blank_rows(fname):
	with open(fname) as f:
		reader = csv.reader(f)
		for line in reader:
			if is_all_blank_line(line):
				continue
			else:
				yield line

with open("00-data.csv", "w") as f:
	writer = csv.writer(f)
	for row in non_blank_rows("00-data-org.csv"):
		writer.writerow(row[:9])