# -*- coding: utf-8 -*-
# Welcome to the DataNitro Editor
# Use Cell(row,column).value, or Cell(name).value, to read or write to cells
# Cell(1,1) and Cell("A1") refer to the top-left cell in the spreadsheet
# 
# Note: To run this file, save it and run it from Excel (click "Run from File")
# If you have trouble saving, try using a different directory


import csv
    
def read_csv(fname):
    with open(fname, 'r') as f:
        reader = csv.reader(f)
        return [row for row in reader]

csv_data = read_csv("invoice.txt")

for r, row in enumerate(csv_data):
    CellRange((r+1, 1), (r+1, len(row))).value = row

CellRange((1,1), (1,len(csv_data[0]))).font.bold = True

last_row =  len(csv_data) + 1

Cell(last_row, 1).value = "Total"
Cell(last_row, 1).font.bold = True

for c in [5, 6, 7]:
    Cell(last_row, c).value = sum(Cell(2, c).vertical)
    Cell(last_row, c).font.bold = True
    

